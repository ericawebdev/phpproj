<?php
    $languageOption = htmlspecialchars($_GET["languageOption"], ENT_QUOTES | ENT_NOQUOTES);
    
    if (strlen($languageOption) < 1 || strlen($languageOption.length) > 100) {
        $languageOption = "JavaScript";
    }

    $service_url = 'https://api.github.com/search/issues?q=' . $languageOption . '+label=bug&sort=created&order=asc';
    $curl = curl_init($service_url);
    curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    $curl_response = curl_exec($curl);
    curl_close($curl);
    return $curl_response;
?>