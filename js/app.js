$(document).ready(function () {
    $("#chooseLanguage").click(function (e) {
        var languageOption = $("#selector").val();
        $("#language").val(languageOption);
        getResults(languageOption);
        e.preventDefault();
    });

    function getResults (languageOption) {
        $.ajax({
        url: "getResults.php?languageOption=" + languageOption})
        .done(function (results) {
            results = JSON.parse(results);
            $("#results").html("");
            for (var i = 0; i < results.items.length; i += 1) {
                $('<li>', {
                    class: 'resultItem',
                    id: 'resultItem' + i
                }).appendTo("#results"); 
                $("#resultItem" + i).html("<a href = '" + results.items[i].url + "'>" + results.items[i].title + " - by " + results.items[i].user.login + "</a>");  
            }
        });
    }
});